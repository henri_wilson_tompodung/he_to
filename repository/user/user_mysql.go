package userRepository

import (
	"database/sql"
	"log"

	"github.com/HeWiTo/users-list/models"
)

type UserRepository struct{}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (u UserRepository) GetUsers(db *sql.DB, user models.User, users []models.User) []models.User {
	rows, err := db.Query("SELECT * FROM users")
	logFatal(err)

	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&user.ID, &user.Fullname, &user.Address, &user.Email, &user.Password)
		logFatal(err)

		users = append(users, user)
	}

	return users
}

func (u UserRepository) GetUser(db *sql.DB, user models.User, id int) models.User {
	rows := db.QueryRow("SELECT * FROM users WHERE id=?", id)

	err := rows.Scan(&user.ID, &user.Fullname, &user.Address, &user.Email, &user.Password)
	logFatal(err)

	return user
}

func (u UserRepository) Login(db *sql.DB, user models.User) (models.User, error) {
	row := db.QueryRow("SELECT * FROM users WHERE email=?", user.Email)
	err := row.Scan(&user.ID, &user.Fullname, &user.Address, &user.Email, &user.Password)

	if err != nil {
		return user, err
	}

	return user, nil
}

func (u UserRepository) CreateUser(db *sql.DB, user models.User) int {
	result, err := db.Exec("INSERT INTO users (fullname, address, email, password) VALUES(?, ?, ?, ?)", user.Fullname, user.Address, user.Email, user.Password)
	logFatal(err)

	id, err := result.LastInsertId()
	logFatal(err)

	return int(id)
}

// func (u UserRepository) Signup(db *sql.DB, user models.User) models.User {
// 	stmt := "INSERT INTO users (fullname, address, email, password) VALUES(?, ?, ?, ?)"
// 	err := db.QueryRow(stmt, user.Fullname, user.Address, user.Email, user.Password).Scan(&user.ID)

// 	logFatal(err)

// 	user.Password = ""
// 	return user
// }

func (u UserRepository) UpdateUser(db *sql.DB, user models.User, id int) int64 {
	result, err := db.Exec("UPDATE users SET fullname=?, address=?, email=?, password=? WHERE id=?", user.Fullname, user.Address, user.Email, user.Password, id)
	logFatal(err)

	rowsUpdated, err := result.RowsAffected()
	logFatal(err)

	return rowsUpdated
}

func (u UserRepository) DeleteUser(db *sql.DB, id int) int64 {
	result, err := db.Exec("DELETE FROM users WHERE id=?", id)
	logFatal(err)

	rowsDeleted, err := result.RowsAffected()
	logFatal(err)

	return rowsDeleted
}
