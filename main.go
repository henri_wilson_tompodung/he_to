package main

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/HeWiTo/users-list/controllers"
	"github.com/HeWiTo/users-list/driver"
	"github.com/HeWiTo/users-list/models"
	"github.com/HeWiTo/users-list/utils"

	"github.com/gorilla/mux"
)

var users []models.User
var db *sql.DB

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	db = driver.ConnectDB()
	router := mux.NewRouter()

	controller := controllers.Controller{}

	router.HandleFunc("/", controller.Home(db)).Methods("GET")
	router.HandleFunc("/registration", controller.Registration(db)).Methods("GET")
	router.HandleFunc("/signup", controller.CreateUser(db)).Methods("POST")
	router.HandleFunc("/login", controller.Login(db)).Methods("POST")

	router.HandleFunc("/users", utils.TokenVerifyMiddleWare(controller.GetUsers(db))).Methods("GET")
	router.HandleFunc("/user/{id}", utils.TokenVerifyMiddleWare(controller.GetUser(db))).Methods("GET")
	router.HandleFunc("/user", utils.TokenVerifyMiddleWare(controller.CreateUser(db))).Methods("POST")
	router.HandleFunc("/user/{id}", utils.TokenVerifyMiddleWare(controller.UpdateUser(db))).Methods("PUT")
	router.HandleFunc("/user/{id}", utils.TokenVerifyMiddleWare(controller.DeleteUser(db))).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", router))
}
