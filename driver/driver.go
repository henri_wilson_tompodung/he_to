package driver

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// ConnectDB is set the database connection
func ConnectDB() *sql.DB {
	dsn := "root:@/rest_api_registration"

	var err error
	db, err = sql.Open("mysql", dsn)
	logFatal(err)

	err = db.Ping()
	logFatal(err)

	return db
}
