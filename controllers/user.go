package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"github.com/HeWiTo/users-list/models"
	"github.com/HeWiTo/users-list/repository/user"
	"github.com/HeWiTo/users-list/utils"
	"golang.org/x/crypto/bcrypt"

	"github.com/gorilla/mux"
)

var users []models.User

type Controller struct{}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) Home(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		files := []string{
			"ui/templates/login.html",
			"ui/templates/layout.html",
			"ui/templates/footer.html",
		}

		ts, err := template.ParseFiles(files...)
		logFatal(err)

		err = ts.Execute(w, nil)
		logFatal(err)
	}
}

func (c Controller) Registration(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		files := []string{
			"ui/templates/registration.html",
			"ui/templates/layout.html",
			"ui/templates/footer.html",
		}

		ts, err := template.ParseFiles(files...)
		logFatal(err)

		err = ts.Execute(w, nil)
		logFatal(err)
	}
}

func (c Controller) GetUsers(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User

		users = []models.User{}
		userRepo := userRepository.UserRepository{}

		users = userRepo.GetUsers(db, user, users)

		json.NewEncoder(w).Encode(users)
	}
}

func (c Controller) GetUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User

		params := mux.Vars(r)

		users = []models.User{}
		userRepo := userRepository.UserRepository{}

		id, err := strconv.Atoi(params["id"])
		logFatal(err)

		user = userRepo.GetUser(db, user, id)

		utils.ResponseJSON(w, user)
	}
}

func (c Controller) CreateUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User
		var error models.Error
		var userID int

		if err := r.ParseForm(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var fullname = r.FormValue("fullname")
		var address = r.FormValue("address")
		var email = r.FormValue("email")
		var password = r.FormValue("password")

		user.Fullname = fullname
		user.Address = address
		user.Email = email
		user.Password = password

		json.NewDecoder(r.Body).Decode(&user)

		if user.Fullname == "" {
			error.Message = "Fullname is missing."
			utils.RespondWithError(w, http.StatusBadRequest, error)
			return
		}

		if user.Address == "" {
			error.Message = "Address is missing."
			utils.RespondWithError(w, http.StatusBadRequest, error)
			return
		}

		if user.Email == "" {
			error.Message = "Email is missing."
			utils.RespondWithError(w, http.StatusBadRequest, error)
			return
		}

		if user.Password == "" {
			error.Message = "Password is missing."
			utils.RespondWithError(w, http.StatusBadRequest, error)
			return
		}

		hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)

		if err != nil {
			log.Fatal(err)
		}

		user.Password = string(hash)

		userRepo := userRepository.UserRepository{}
		userID = userRepo.CreateUser(db, user)

		if err != nil {
			error.Message = "Server error."
			utils.RespondWithError(w, http.StatusInternalServerError, error)
			return
		}

		user.Password = ""

		message := fmt.Sprintf("Hello %s, thank you for joining us", user.Fullname)
		w.Write([]byte(message))

		w.Header().Set("Content-Type", "application/json")
		utils.ResponseJSON(w, userID)
	}
}

func (c Controller) UpdateUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User
		json.NewDecoder(r.Body).Decode(&user)

		params := mux.Vars(r)

		userRepo := userRepository.UserRepository{}

		id, err := strconv.Atoi(params["id"])
		logFatal(err)

		rowsUpdated := userRepo.UpdateUser(db, user, id)

		json.NewEncoder(w).Encode(rowsUpdated)
	}
}

func (c Controller) DeleteUser(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)

		userRepo := userRepository.UserRepository{}

		id, err := strconv.Atoi(params["id"])
		logFatal(err)

		rowsDeleted := userRepo.DeleteUser(db, id)

		json.NewEncoder(w).Encode(rowsDeleted)
	}
}
