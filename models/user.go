package models

type User struct {
	ID       int    `json:"id"`
	Fullname string `json:"fullname"`
	Address  string `json:"address"`
	Email    string `json:"email"`
	Password string `json:"password"`
}
